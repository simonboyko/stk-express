document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var scroll = new SmoothScroll('a[href*="#"]', {
    ignore: '[data-modal-button]'
  });

  addBackToTop();
});