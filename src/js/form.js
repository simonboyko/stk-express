function thankyou(form, answerWrp) {
  // console.log('CALL thankyou');
  // form.setAttribute('data-hidden-form', '');
  answerWrp.innerHTML = '<div class="form__text">Спасибо! Мы получили все ваши данные и свяжемся с вами в ближайшее время.</div>'
}

// form
document.addEventListener('DOMContentLoaded', function () {
  'use strict';
  console.info('INIT form.js');

  var form = document.querySelector('.form.-call');
  var formRegister = document.querySelector('.form.-form-register');
  var formConsultation = document.querySelector('.form.-form-consultation');
  var formCounter = 0;
  var answerWrp = document.querySelector('.orderPlace__form');
  // var formItems = form.querySelectorAll('.form__item');


  function sendForm(method, action, form,  answerWrp) {
    var xhr = new XMLHttpRequest();
    var FD = new FormData(form);


    if (!xhr) {
      console.error('Don\'t support XMLHttpRequest');
      return false;
    }

    xhr.addEventListener('load', function(e) {
      thankyou(form, form);
    });

    xhr.addEventListener('error', function(e) {
      console.error('Error with sending form!');
    });

    xhr.open(method, action);

    xhr.send(FD);


  }

  function sendFormCall(method, action, form, answerWrp) {
    var xhr = new XMLHttpRequest();
    formCounter++;

    sendForm(method, action, form);

    xhr.addEventListener('load', function(e) {
      if (formCounter === 2) {
        thankyou(form, answerWrp);
      }
    });

  }

  function toggleFields(e) {
    var form = e.currentTarget;
    var formItems = form.querySelectorAll('.form__item');
    var method = form.getAttribute('method');
    var action = form.getAttribute('action');

    for (var i = 0; i <formItems.length;i++) {
      var formItem = formItems[i];

      if (formItem.getAttribute('data-hidden-form') !== null) {
        formItem.removeAttribute('data-hidden-form');
      } else {
        formItem.setAttribute('data-hidden-form', '');
      }
    }

    sendFormCall(method, action, form, answerWrp);

    e.preventDefault();
    form.addEventListener('submit', function (e) {
      e.preventDefault();
      sendFormCall(method, action, form);
    })
  }

  form.addEventListener('submit', toggleFields, {once: true});
  formRegister.addEventListener('submit', function (e) {
    var form = e.currentTarget;
    var method = form.getAttribute('method');
    var action = form.getAttribute('action');

    e.preventDefault();
    sendForm(method, action, form);
  });
  formConsultation.addEventListener('submit', toggleFields, function (e) {
    var form = e.currentTarget;
    var method = form.getAttribute('method');
    var action = form.getAttribute('action');

    e.preventDefault();
    sendForm(method, action, form, form);
  });

});