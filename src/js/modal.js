document.addEventListener('DOMContentLoaded', function () {
  'use strict';
  console.info('INIT modal.js');

  var modal = document.querySelectorAll('.modal');
  var link = document.querySelectorAll('[data-modal-button]');
  var closer = document.querySelectorAll('[href="#close"]');
  var scrollbarWidth = window.innerWidth - document.body.clientWidth;

  function scrollDisable() {
    document.body.classList.add('scrollLock');
    document.body.style.cssText = 'border-right: ' + scrollbarWidth + 'px solid transparent';
  }

  function scrollEnable() {
    document.body.classList.remove('scrollLock');
    document.body.style.cssText = '';
  }

  for (var i = 0; i < modal.length; i++)
  modal[i].addEventListener('click', function (e) {
    if (this === e.target) {
      window.location.href = '#close';
      scrollEnable();
    }
  });

  for (i = 0; i < closer.length; i++)
    closer[i].addEventListener('click', scrollEnable)

  for (i = 0; i < link.length; i++)
    link[i].addEventListener('click', scrollDisable);
});