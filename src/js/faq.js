document.addEventListener('DOMContentLoaded', function () {
  'use strict';

  var itemClass = '.faq__item';
  var faqItem = document.querySelectorAll(itemClass);

  function toggleClass(e) {
    var el = e.currentTarget;
    var elActive = document.querySelector(itemClass + '.-active');

    if (elActive) {
      elActive.classList.remove('-active');
      if (el === elActive) return;
    }

    el.classList.add('-active');

  }

  for (var i = 0; i<faqItem.length;i++) {
    faqItem[i].addEventListener('click', toggleClass);
  }

  console.info('Init faq.js');
});