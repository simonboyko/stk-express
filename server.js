const express = require('express');
const multer = require('multer');
const mailer = require('./send-mail');
const app = express();
const upload = multer();
const db = require('./data-base');

const public = '/build';

app.use(upload.array());
app.use(express.static(__dirname + public));

app.get('/', (req, res) => res.sendfile(__dirname + public + '/index.html'));

app.post('/', (req, res) => {
  res.status(200);

  for (let i in req.body) {
    res.write(i + ': ' + req.body[i] + '<br>');
  }

  res.send();

  db.write(req.body);

  let clientEmail = req.body['email'];
  mailer.sendMail(req.body);
  if (clientEmail) mailer.sendToClient(clientEmail);

});

app.listen(8083, () => console.log('Example app listening on port 8083!'));


