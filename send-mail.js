'use strict';
const nodemailer = require('nodemailer');
const fs = require('fs');
const logFile = '/email.log';


// settings
const fromSettings = {
  host: 'smtp.yandex.ru',
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: 'noreply@studienkolleg-express.ru',
    pass: 'ZQB7e5Ausd1X',
  },
};
const from = 'STK Express \<noreply@studienkolleg-express.ru\>';


const sendMail = (body) => {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport(fromSettings);

  const parseData = (body) => {
    let formTitle = body['form-title'];
    let html = '<h2>Заполнена форма «' + formTitle + '»</h2>';
    for (let key in body) {
      let label = key;
      let value = body[key];

      if (value !== '' && value !== 'unselect' && label !== 'fz152') {
        html = html + `<b>${label}:</b> ${value}<br>`;
      }

    }
    return html;
  };

  let mailOptions = {
    from: from, // sender address
    to: 'info@studienkolleg-express.ru', // list of receivers
    bcc: 'simonderus@gmail.com',
    subject: 'Заполнена форма на сайте', // Subject line
    // text:  // plain text body
    html: parseData(body) // html body
  };

  transporter.sendMail(mailOptions, (error, info) => {
    var date = new Date();
    if (error) {
      fs.addendFile(__dirname + logFile, `[${date}] ERROR! Mail is not send`, options, (err) => {
        if (err) throw err;
      });
      return console.log(error);
    } else {
      fs.addendFile(__dirname + logFile, `[${date}] Send mail`, options, (err) => {
        if (err) throw err;
      });
      return console.log('Mail send');
    }
  });

};

const sendToClient = (sendTo) => {
  let transporter = nodemailer.createTransport(fromSettings);

  let mailOptions = {
    from: from, // sender address
    to: sendTo, // list of receivers
    subject: 'Запись на курс «Подготовка к вступительному экзамену в Studienkolleg»',
    html: '<h2>Спасибо за заявку!</h2>\n' +
    '<p>Мы свяжемся с вами в блишайшее время</p>',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
  });
};

module.exports = {sendMail, sendToClient};

