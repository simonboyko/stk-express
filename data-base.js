const fs = require('fs');
const FILE = '/db.json';
const options = {encoding: 'utf8'};

const write = (data) => {
  console.log(data);
  fs.appendFile(__dirname + FILE, JSON.stringify(data), options, (err) => {
    if (err) throw err;
    console.log('Add a new record to DB');
  })
};

module.exports = {write};
